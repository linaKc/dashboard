var path = require('../')

module.exports = ((app, passport) => {
    app.get('/about.js', (req, res) => {
        res.sendFile('about.json', { root: '.'});
    })
})
var path = require('../');
const request = require('request');
const argv = require('yargs').argv;

var city = argv.c || 'paris';
const weatherAPI = "5e8c00555106d512e5a9d6353474c11e";
const weatherURL = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${weatherAPI}`

module.exports = function(app, passport) {
    let data = [{
        services: false,
        isWeather: false,
    }]

	app.get('/dashboard', (req, res) =>  {
        console.log("GET /dasboard#tes in routes!  ");
        let weather = (!req.body.weather ? null : req.body.weather);
        console.log("In GET: ", req.body);
        res.render('dashboard', {home: true, weather: weather, error: false});
    })

	app.post('/dashboard', (req, res) =>  {
        let te = (!req.body.weather ? null : req.body.weather);
        console.log("In POST: ", req.body);
        res.render('dashboard', {home: true, weather: te, error: false});
    })

    app.get('/dashboard/services', (req, res) => {
        res.render('services', {addService: true});
    })
}
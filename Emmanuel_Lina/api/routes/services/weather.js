var path = require('../../');
const request = require('request')
let APIKey = "5e8c00555106d512e5a9d6353474c11e";

/*  TEMPLATE to service route

    app.get('/dashboard/services/youtube', (req, res) => {
        res.render('services/youtube', {
            home: null,
            services: true,
            weather: null,
            youtube: null;
        });
 */

module.exports = (app, pass) => {
    app.get('/dashboard/services/weather', (req, res) => {
        res.render('services/weather', {
            home: null,
            services: true,
            inService: true,
            error: null,
        });
    })

    app.post('/dashboard/services/weather', (req, res, next) => {
        let city = req.body.city;
        let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${APIKey}`;

        request(url, (err, response, body) => {
            if (err)
                res.render('weather', {
                    home: null,
                    services: true,
                    inService: true,
                    error: 'Error, please try again.'
                });
            else {
                let weather = JSON.parse(body)
                if (weather.main == undefined)
                    res.render('services/weather',{
                        home: null,
                        services: true,
                        inService: true,                    
                        error: 'Error, city weather not found.'
                });
                else {
//                    var weatherText = `It's ${weather.weather[0].description}.
//The temperature is ${weather.main.temp}°C in ${weather.name}, ${weather.sys.country}!`;
                    req.session.weather = {
                        sky: weather.weather[0].description,
                        temp: weather.main.temp,
                        city: weather.name,
                        country: weather.sys.country
                    };
                    res.redirect('/dashboard');
                }
            }
        })
    })
}

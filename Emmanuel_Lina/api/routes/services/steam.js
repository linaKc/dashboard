var path = require('../../');
const request = require('request')
const SteamAPI = require('steamapi')
//const steam = new SteamAPI('F81781922871D3D7207AB31C4C1E514D')
//const steamID = '76561198869972883';

console.log("ICI")
/*  TEMPLATE to service route

    app.get('/dashboard/services/youtube', (req, res) => {
        res.render('services/youtube', {
            home: null,
            services: true,
            inService: true,
        });
 */

module.exports = (app, pass) => {
    app.get('/dashboard/services/steam', (req, res) => {
//        steam.resolve(`https://steamcommunity.com/id/Tiger84`)
//        .then(id => {
//            console.log(id); // 76561198869972883
//        })
//        .catch(err => console.log(err));
//    
//        steam.getAppList().then(apps => {
//            console.log(apps);
//        })
//        .catch(err => console.log(err))

        res.render('services/steam', {
            home: null,
            services: true,
            inService: true,
            errors: null,
        });
    })

    const getGameID = async (steam, name) => {
        return new Promise((success, failure) => {
            steam.getAppList().then(apps => {
                for (const [key, value] of Object.entries(apps)) {
                    console.log(value.name);
                    if (name === value.name) {
                        console.log("MATCH!", value.name, value.appid);
                        success(value.appid);
                    }
                }
            })
            .catch(err => failure(err));
        })
    }
    

    app.post('/dashboard/services/steam', (req, res) => {
        let steam = new SteamAPI('F81781922871D3D7207AB31C4C1E514D');
        let url = 'https://steamcommunity.com/id/' + req.body.url;

        steam.resolve(url).then(id => {
            steam.getUserSummary(id)
            .then(summary => {/*
                const gameID = getGameID(steam, req.body.name)
                .then(data => {
                    console.log(data);
                })
                .catch(err => {
                    console.log("ERR: ", err);
                })
                console.log(summary);
                setTimeout(() => {
                    console.log(gameID);
                }, 5000);
                */
                let createdDate = (new Date(summary.created * 1000)).toLocaleDateString('fr-FR');
                let lastDate = (new Date(summary.lastLogOff * 1000)).toLocaleDateString('fr-FR');

                req.session.steam = {
                    name: summary.nickname,
                    created: createdDate,
                    lastLogin: lastDate,
                }

                res.redirect('/dashboard');
            })
            .catch(err => res.render('services/steam', {
                home: null,
                services: true,
                inService: true,
                errors: 'Error: Invalid Steam custom URL.',                
            }));
        })
        .catch(err => res.render('services/steam', {
            home: null,
            services: true,
            inService: true,
            errors: 'Error: Invalid Steam custom URL.',                
        }));
    })
}


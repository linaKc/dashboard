var path = require('../');
const request = require('request');
const argv = require('yargs').argv;

var city = argv.c || 'paris';
const weatherAPI = "5e8c00555106d512e5a9d6353474c11e";
const weatherURL = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${weatherAPI}`

module.exports = function(app, passport) {
    let data = [{
        services: false,
        isWeather: false,
    }]

	app.get('/dashboard', (req, res) =>  {
        console.log("/dashobard GET Home page!");
        let weather = (!req.session.weather ? null : req.session.weather);
        let steam = (!req.session.steam ? null : req.session.steam);
        res.render('dashboard', {
            home: true,
            services: null,
            profile: null,
            weather: weather,
            steam: steam,
            error: false,
        });
    })

//    function mainPage(req, res) {
//        let te = (!req.session.weather ? null : req.session.weather);
//        console.log("In POST: ", req.session);
//        res.render('dashboard', {home: true, weather: te, error: false});
//    }
//
//	app.post('/dashboard', mainPage);

    app.get('/dashboard/services', (req, res) => {
        res.render('services/services', {
            home: null,
            services: true,
            profile: false,
            weather: null,
            error: false,
        });
    })
}
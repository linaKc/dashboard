var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var User = require('../models/user');
var configAuth = require('./auth');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
passport.use(new GoogleStrategy({
    clientID: configAuth.google.clientID,
    clientSecret: configAuth.google.clientSecret,
    callbackURL: configAuth.google.callbackURL,
    passReqToCallback: true
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function() {
        User.findOne({'google.id': profile.id}, function(err, user) {
            if(err)
                return done(err);
            else if(user)
                return done(null, user);
            else {
                var newUser = new User();
                newUser.google.id = profile.id;
                newUser.google.token = accessToken;
                newUser.google.name = profile.displayName;
                newUser.google.email = profile.emails[0].value;

                newUser.save(function(err){
                    if(err)
                        throw err;
                    return done(null, newUser);
                })
                console.log(profile);
            }
        });
    });
}
));
}
var passport = require('passport');
//var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var User = require('../models/user');
var configAuth = require('./auth');

module.exports = function(passport) {
	passport.use(new FacebookStrategy({
	    clientID: configAuth.facebook.clientID,
	    clientSecret: configAuth.facebook.clientSecret,
	    callbackURL: configAuth.facebook.callbackURL
	  },
	  function(accessToken, refreshToken, profile, done) {
	    	process.nextTick(function() {
	    		User.findOne({'facebook.id': profile.id}, function(err, user) {
	    			if (err)
	    				return done(err);
	    			else if (user)
	    				return done(null, user);
	    			else {
	    				var newUser = new User();
	    				newUser.facebook.id = profile.id;
	    				newUser.facebook.token = accessToken;
	    				newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
	    				newUser.facebook.email = profile.emails[0].value;

	    				newUser.save(function(err){
	    					if(err)
	    						throw err;
	    					return done(null, newUser);
	    				})
	    				console.log(profile);
	    			}
	    		});
	    	});
	    }

	));
//
//	passport.use(new GoogleStrategy({
//		consumerKey: configAuth.google.clientID,
//		consumerSecret: configAuth.google.clientSecret,
//		callbackURL: configAuth.google.callbackURL
//	  },
//	  function(accessToken, refreshToken, profile, done) {
//		process.nextTick(function() {
//			User.findOne({'google.id': profile.id}, function(err, user) {
//				if(err)
//					return done(err);
//				else if(user)
//					return done(null, user);
//				else {
//					var newUser = new User();
//					newUser.google.id = profile.id;
//					newUser.google.token = accessToken;
//					newUser.google.name = profile.displayName;
//					newUser.google.email = profile.emails[0].value;
//	
//					newUser.save(function(err){
//						if(err)
//							throw err;
//						return done(null, newUser);
//					})
//					console.log(profile);
//				}
//			});
//		});
//	}
//	));
//
};
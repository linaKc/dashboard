var express = require('express');
var app = express();
var port = process.env.PORT || 8080;

var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');

var configDB = require('./config/db.js');
mongoose.connect(configDB.url, { useNewUrlParser: true,
								useUnifiedTopology: true });
//require('./api/config/facebookPass')(passport);
//require('./api/config/googlePass')(passport);
require('./config/localPass')(passport);

mongoose.connect(configDB.url, { useNewUrlParser: true,
								useUnifiedTopology: true });

app.use(express.static('./public'));
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(session({secret: 'anystringoftext',
saveUninitialized: true,
resave: true}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
//googleStrategy(passport); 

app.set('view engine', 'ejs');
app.set('views', './views');

require('./routes/auth.js')(app, passport);
require('./routes/about.js')(app);
require('./routes/dashboard.js')(app, session);
require('./routes/services/weather.js')(app, passport);
require('./routes/services/steam.js')(app, passport);

app.listen(port);
console.log('Server running on port: ' + port);